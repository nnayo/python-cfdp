# Examples

This folder contains examples for using the CFDP package.

First, start a a remote entity (server):

```
$ cd <example-folder>
$ python server.py
```

Then run one of the local entity (client) examples:

```
$ cd <example-folder>
$ python client_send_file_class1.py
```
